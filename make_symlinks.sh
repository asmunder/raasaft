#!/bin/bash

# This script sets up the symlinks you need for the import alkane_mie etc.

# The optional argument is the target directory, so the following are equivalent
#
# $ cd directory
# $ make_symlinks.sh
#
# $ make_symlinks.sh directory

# Handle the two cases:
if [[ $1 == "" ]]; then
  target="."
else
  target=$1
fi

# First determine how far up the main raasaft directory is
raadir="${PWD}"
# While directory string is not empty
while [ -n "${raadir}" ]; do
   # If this directory does not contain the actual mie.py (not counting symlinks) 
    if [[ ! -e ${raadir}/"mie.py" || -h ${raadir}/"mie.py" ]]; then
      # Remove trailing directory from directory string
      raadir=${raadir%/*}
    else
      break
    fi
done

# Now make the symlinks

# General stuff
ln -sf $raadir/mie.py $target/
ln -sf $raadir/utility.py $target/
ln -sf $raadir/constants.py $target/

# All the specific models
for model in `ls $raadir/*_mie.py`; do
  ln -sf $model $target/
done
