from hoomd_script import *
from raasaft.main import *
from raasaft.water import *
import math

# HOOMD-blue 1.1 and newer require this
context.initialize()

# define the temperature, time step, number of steps etc.
TempKelvin=293.15
Temp = TempKelvin*kBby10 # converted to HOOMD temperature units (energy)
TempSupercritKelvin=700.0
TempSupercrit = TempSupercritKelvin*kBby10
tauTemp = 0.5 # in picoseconds
Nrelax = 1e3
Nsupercrit = 1e5
Ncooldown = 1e3
Nresize = 1e3
Nsteps = 1e6
int_dt = 0.001 # time step length in picoseconds

# set up the component
H2O = BioWater(count=4e4)
components = [H2O] 

# set up the box
theBox = setupSimBox(components,elong=3.0,packing=0.1)

# set up the simulation, i.e. initialize positions and bonds for all beads
system = setupSimulation(components,theBox)

# tabulate the Mie potential and force for the selected molecules
setCutoff(components)
mie = pair.table(width=1000)
setPotentialCoeffs(components,mie)

# set up the time integration
all = group.all()
integrate.mode_standard(dt=int_dt)

# set up file outputs: xml file with initial configuration and bonds, angles
# etc. and a dcd file with 100 snapshots during the relax/supercritical/resize
xml = dump.xml(filename='output/ex2_init.xml', vis=True)
dump.dcd(filename='output/ex2_init.dcd', period=1e3)

# integrate first in the NVE ensemble with limiter to relax the system
relaxation = integrate.nve(group=all, limit=0.01)
run(Nrelax)
relaxation.disable()

# create a variant that runs at supercritical temp for some time, and then
# lowers the temperature to 20 C, then increases to 30C after Nsteps, etc.
TempChanger = variant.linear_interp(points = [
    (0, TempSupercrit), # start at supercritical temp 
    (Nsupercrit,TempSupercrit), # maintain for Nsupercrit steps 
    (Nsupercrit+Ncooldown,Temp), # cool down to target Temp in Ncooldown steps
    (Nsupercrit+Ncooldown+Nresize+1*Nsteps+0*Ncooldown,Temp), # maintain
    (Nsupercrit+Ncooldown+Nresize+1*Nsteps+1*Ncooldown,Temp+10*kBby10), # Heat by 10 K
    (Nsupercrit+Ncooldown+Nresize+2*Nsteps+1*Ncooldown,Temp+10*kBby10), # Maintain
    (Nsupercrit+Ncooldown+Nresize+2*Nsteps+2*Ncooldown,Temp+20*kBby10), # Heat by 10 K
    (Nsupercrit+Ncooldown+Nresize+3*Nsteps+2*Ncooldown,Temp+20*kBby10), # Maintain
    (Nsupercrit+Ncooldown+Nresize+3*Nsteps+3*Ncooldown,Temp+30*kBby10), # Heat by 10 K
    (Nsupercrit+Ncooldown+Nresize+4*Nsteps+3*Ncooldown,Temp+30*kBby10), # Maintain
    (Nsupercrit+Ncooldown+Nresize+4*Nsteps+4*Ncooldown,Temp+40*kBby10), # Heat by 10 K
    (Nsupercrit+Ncooldown+Nresize+5*Nsteps+4*Ncooldown,Temp+40*kBby10), # Maintain
    (Nsupercrit+Ncooldown+Nresize+5*Nsteps+5*Ncooldown,Temp+50*kBby10), # Heat by 10 K
    (Nsupercrit+Ncooldown+Nresize+6*Nsteps+5*Ncooldown,Temp+50*kBby10), # Maintain
    (Nsupercrit+Ncooldown+Nresize+6*Nsteps+6*Ncooldown,Temp+60*kBby10), # Heat by 10 K
    (Nsupercrit+Ncooldown+Nresize+7*Nsteps+6*Ncooldown,Temp+60*kBby10), # Maintain
    (Nsupercrit+Ncooldown+Nresize+7*Nsteps+7*Ncooldown,Temp+70*kBby10), # Heat by 10 K
    (Nsupercrit+Ncooldown+Nresize+8*Nsteps+7*Ncooldown,Temp+70*kBby10) # Maintain
    ])


# switch to NVT
integrate.nvt(group=all, T=TempChanger, tau=tauTemp)
# first run supercritical to disorder the system, then cool down
run(Nsupercrit)
run(Ncooldown)

# shrink the box to get desired packing
resizeFactor=1.0/2.2
resizer = resizeBox(theBox,resizeFactor,0,Nresize,10)
# resize run
run(Nresize)
resizer.disable

# set up file outputs: xml file and a dcd file with 10 snapshots per 
# temperature for the production run.
xml = dump.xml(filename='output/ex2_prod.xml', vis=True)
dump.dcd(filename='output/ex2_prod.dcd', period=math.floor(Nsteps/10))

# setup logging of global quantities every 1000 time steps
logQuant = ['time', 'pair_table_energy', 'pressure', 'temperature', 'pressure_xx', 'pressure_yy', 'pressure_zz']
analyze.log(filename='output/ex2_prod_log.dat', quantities=logQuant, period=1e3, header_prefix='')

# then do the production run
run(Nsteps*8+Ncooldown*7)
# dump an xml file that we can restart from
xml = dump.xml(filename='output/ex2_restart.xml', vis=True)
