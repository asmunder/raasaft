## Summary

This directory contains examples intended to get you started with coarse-grained
simulations using the raaSAFT framework.

.
├── README.txt                           # This document
├── make_symlinks.sh                     # Script to create all the symlinks
├── ex1.hoomd                            # The examples (see below)
├── ex2.hoomd
├── ex3.hoomd
├── output                               # Directory for output from examples

### VLE (vapor-liquid equilibrium) examples

* ex1.hoomd:
  Basic example of a single component developing equilibrium with it's own
  vapor. This case is very useful to test single component densities, surface
  tension etc.

* ex2.hoomd:
  Extended ex1 where a temperature sweep is performed, such that one may
  obtain the surface tension, densities etc. at a range of temperatures.

### LLE (liquid-liquid equilibrium) examples

* ex3.hoomd:
  Basic example of a two-component liquid mixture. Detailed explanation of how
  to get the correct pressure etc., and how to speed up the phase separation.


