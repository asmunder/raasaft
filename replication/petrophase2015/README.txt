Author: Åsmund Ervik
Date: 11 June 2015

The simulation file in this folder is the basis for the simulations I did for my
presentation at Petrophase 2015. It gives the initial simulation of
setup + 5 ns of simulation for either of the two concentrations I considered (240
or 720 asphaltene molecules). You may restart longer simulations from these,
and you may do volume-preserving box deformations etc.
